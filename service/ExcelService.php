<?php
/**
 * mudanlian
 *
 * User: LLH
 * Date: 2018/6/11
 * Time: 16:03
 */

namespace sealen\service;


class ExcelService
{
    /**
     * 导出文件
     *
     * @param  array  $params   excel 头部 及其对应的data key ：  ['nickname' => 昵称]：
     * @param  array  $data
     * @param  string $savePath 下载excel 的路径 :   'D:\\abc\doc/excel/'
     * @param string  $name     保存文件名
     *
     * @return array   ['code' => 0 ,'msg' =>'smg','data' => $data] 返回信息  code: 0 时处理成功
     */
    function export(array $params , array $data , $savePath , $name = '')
    {
        $objPHPExcel = new \PHPExcel();
        $header = ['A' , 'B' , 'C' , 'D' , 'E' , 'F' , 'G' , 'H' , 'I' , 'J' , 'K' , 'L' , 'M' , 'N' , 'O' , 'P' , 'Q' , 'R' , 'S' , 'T' , 'U' , 'V' , 'W' , 'X' , 'Y' , 'Z'];
        $downloadPath = $savePath;
        $type = '.xls';

        $I = 0;
        $config = [];
        foreach ($params as $key => $val){
            $objPHPExcel->getActiveSheet()->setCellValue($header[$I]. 1 , $val);
            $config[$header[$I]] = $key;
            $I++;
        }

        foreach ($data as $key => $val){
            $i = 0;
            foreach ($config as $k => $v){
                $objPHPExcel->getActiveSheet()->setCellValue($header[$i].($key + 2) , $val[$config[$header[$i]]]);
                $i++;
            }
        }

        $objWriter = new \PHPExcel_Writer_Excel5($objPHPExcel);
        if (empty($name)){
            $name = date('YmdHis');
        }
        if (!is_dir($downloadPath)){
            mkdir($downloadPath , 0775);
        }

        $filePath = $downloadPath.$name.$type;
        $objWriter->save($filePath);

        $ret['fileName'] = $name.$type;
        $ret['name'] = $name;
        return ['code' => 0 , 'msg' => '保存成功' , 'data' => $ret];
    }

    /**
     * 分页下载
     *
     * @param        $params
     * @param        $data
     * @param        $runtimePath
     * @param        $savePath
     * @param string $name
     *
     * @return array
     */
    function pageExport($params , $data , $runtimePath , $savePath , $name = '')
    {
        $header = ['A' , 'B' , 'C' , 'D' , 'E' , 'F' , 'G' , 'H' , 'I' , 'J' , 'K' , 'L' , 'M' , 'N' , 'O' , 'P' , 'Q' , 'R' , 'S' , 'T' , 'U' , 'V' , 'W' , 'X' , 'Y' , 'Z'];
        $objPHPExcel = new \PHPExcel();
        $type = '.xls';

        if (!empty($data)){
            $rows = 0;
            if (!empty($name)){
                if (file_exists($runtimePath.$name.$type)){
                    $objReader = \PHPExcel_IOFactory::createReader('Excel5');
                    $objPHPExcel = $objReader->load($runtimePath.$name.$type);

                    $sheet = $objPHPExcel->getSheet(0);
                    $rows = $sheet->getHighestRow(); // 取得总行数
                    if ($rows > 1){
                        $rows -= 1;
                    }
                }
            }
            $I = 0;
            $config = [];
            foreach ($params as $key => $val){
                $objPHPExcel->getActiveSheet()->getColumnDimension($header[$I]);
                if ($rows == 0){
                    $objPHPExcel->getActiveSheet()->setCellValue($header[$I]. 1 , $val);
                }
                $config[$header[$I]] = $key;
                $I++;
            }

            foreach ($data as $key => $val){
                $i = 0;
                foreach ($config as $k => $v){
                    $objPHPExcel->getActiveSheet()->setCellValue($header[$i].($key + 2 + $rows) , $val[$config[$header[$i]]]);
                    $i++;
                }

            }

            $objWriter = new \PHPExcel_Writer_Excel5($objPHPExcel);
            if (empty($name)){
                $name = date('YmdHis');
            }

            if (!is_dir($runtimePath)){
                mkdir($runtimePath , 0775);
            }
            if (empty($name)){
                $name = date('YmdHis');
            }

            $objWriter->save($runtimePath.$name.$type);
            $ret['fileName'] = $name.$type;
            $ret['name'] = $name;

            return ['code' => 1 , 'msg' => '分页写入完成' , 'data' => $ret];
        }else{

            if (!is_dir($savePath)){
                mkdir($savePath , 0775);
            }

            if (!copy($runtimePath.$name.$type , $savePath.$name.$type)){
                return ['code' => 2 , 'msg' => '文件转移失败:'.$runtimePath.$name.$type.'  ->  '.$savePath.$name.$type , 'data' => []];
            }

            if (is_file($runtimePath.$name.$type)){
                unlink($runtimePath.$name.$type);
            }

            $ret['fileName'] = $name.$type;
            $ret['name'] = $name;

            return ['code' => 0 , 'msg' => '分页写入完成' , 'data' => $ret];
        }
    }
}